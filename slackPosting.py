import os
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
import logging
from bot_data import TOKEN, APP_LEVEL_TOKEN

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Define Token
token = TOKEN
appLevelToken = APP_LEVEL_TOKEN
client = WebClient(token=token)

def postMessage(text,channel_id):
    try:
        # Call the chat.postMessage method using the WebClient
        result = client.chat_postMessage(
            channel=channel_id, 
            text=text
        )
        logger.info(result)
        print ("Message sent!")
    except SlackApiError as e:
        logger.error(f"Error posting message: {e}")

def postMessageSilently(text,channel_id,user_id):
    try:
        # Call the chat.postMessage method using the WebClient
        result = client.chat_postEphemeral(
            channel=channel_id, 
            text=text,
            user=user_id
        )
        logger.info(result)
        print ("Silent message sent!")
    except SlackApiError as e:
        logger.error(f"Error posting message: {e}")

def postFile(filePath,channel_id,title):
    try:
        # Call the chat.postMessage method using the WebClient
        result = client.files_upload(
            channels=channel_id,
            file=filePath,
            title=title
        )
        logger.info(result)
        print ("File sent!")
    except SlackApiError as e:
        logger.error(f"Error posting message: {e}")

def listUsers():
    users_store = {}
    # Put users into the dict
    def save_users(users_array):
        print ("Total users:" + str(len(users_array)))
        for user in users_array:
            # Key user info on their unique user ID
            user_id = user["id"]
            user_profile = user["profile"]
            user_name = user_profile['real_name']

            # Store the entire user object (you may not need all of the info)
            users_store[user_name] = {'user_name':user_name,'id':user_id}

    try:
        # Call the users.list method using the WebClient
        # users.list requires the users:read scope
        result = client.users_list()
        save_users(result["members"])

    except SlackApiError as e:
        logger.error("Error creating conversation: {}".format(e))

    return users_store


if __name__ == '__main__':
    # -- RUN -- postMessage
    # Define Text
    filePath = "X:/somePath/someFile.ma"
    step = "MOD"
    user = os.environ.get( "USERNAME" ) 
    text = "New {1} publish file: {0}\nAuthor: {2}".format(filePath,step, user)
    # ID of the channel you want to send the message to
    channel_id = "C02BJLFKL4U"      #Testing
    user_id = "U0250SPA4AZ"         #User LucasMorante

    postMessage(text,channel_id)

    # -- RUN -- listUsers
    """
    users = {}
    users = listUsers()

    keys=users.keys()
    for key in keys:
        print(users[key])
    print (len(keys))
    """