# https://slack.dev/python-slack-sdk/web/index.html#messaging
#### ----------------------------------------------------------------------------
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
import logging
import os
from slack_bolt import App
from slack_bolt.adapter.socket_mode import SocketModeHandler
from bot_data import TOKEN, APP_LEVEL_TOKEN

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Define Token
token = TOKEN
appLevelToken = APP_LEVEL_TOKEN
client = WebClient(token=token)

channel_id = "C02BJLFKL4U"      #Testing
user_id = "U0250SPA4AZ"         #User LucasMorante
#### ----------------------------------------------------------------------------

def postMessage(text,channel_id):
    try:
        # Call the chat.postMessage method using the WebClient
        result = client.chat_postMessage(
            channel=channel_id, 
            text=text
        )
        logger.info(result)
        print ("Message sent!")
    except SlackApiError as e:
        logger.error(f"Error posting message: {e}")

def postMessageSilently(text,channel_id,user_id):
    try:
        # Call the chat.postMessage method using the WebClient
        result = client.chat_postEphemeral(
            channel=channel_id, 
            text=text,
            user=user_id
        )
        logger.info(result)
        print ("Silent message sent!")
    except SlackApiError as e:
        logger.error(f"Error posting message: {e}")

def postFile(filePath,channel_id,title):
    try:
        # Call the chat.postMessage method using the WebClient
        result = client.files_upload(
            channels=channel_id,
            file=filePath,
            title=title
        )
        logger.info(result)
        print ("File sent!")
    except SlackApiError as e:
        logger.error(f"Error posting message: {e}")

#RUN
text = "This is a test! :+1:"
assetName = "Lewis"
publishFileName = "XGA_char_lewis_rig_final_main_043.ma"
#postMessageSilently(text,channel_id,user_id)
"""
result = client.chat_postMessage(
    channel=channel_id,
    blocks=[
		{
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": "New publish of *{0}*: {1}".format(assetName,publishFileName)
			}
		},
		{
			"type": "actions",
			"elements": [
				{
					"type": "button",
					"text": {
						"type": "plain_text",
						"text": "Open Wiki"
					},
					"value": "click_me_123",
                    "url": "https://illusoriumstudios.notion.site/Illusorium-Wiki-0a94906bc6ee4e1b99bb28fac2705397"
				},
				{
					"type": "button",
					"text": {
						"type": "plain_text",
						"text": "Scene"
					},
					"value": "click_me_123"
				},
				{
					"type": "button",
					"text": {
						"type": "plain_text",
						"text": "Media"
					},
					"value": "click_me_123",
					"url": "https://google.com"
				},
				{
					"type": "button",
					"text": {
						"type": "plain_text",
						"text": "Cache"
					},
					"value": "click_me_123",
					"url": "https://google.com"
				}
			]
		}
    ]
)

print (result)

GUID = "cfe11c6fc822422788b3cdc0d279cf44"
file_url = "file:///C:/Users/LucasMorante/Desktop/Black.jpg"
file_title = "DemoFile"

GUID = "5f8cffc4603144238aacef3f7159b3b8"
file_url = "https://drive.google.com/file/d/1A9MMQBjva13eIOSUq3AXPIqopxrFTwVM/view?usp=sharing"
file_title = "spiderVerse"

GUID = "d157c97b5f4f408eabb6087ab49c33ee"
file_url = "https://drive.google.com/file/d/1A9MMQBjva13eIOSUq3AXPIqopxrFTwVM/view?usp=sharing"
file_title = "fileTest"

result = client.files_remote_add(token=token, external_id=GUID, external_url=file_url, title=file_title, )
print (result)

resultShare = client.files_remote_share(token=token, external_id=GUID, channels=channel_id, title=file_title)
print (resultShare)
"""

os.environ["SLACK_BOT_TOKEN"] = token
os.environ["SLACK_APP_TOKEN"] = appLevelToken

app = App(token=os.environ.get("SLACK_BOT_TOKEN"))

@app.message(":wave:")
def say_hello(message, say):
    user = message['user']
    say(f"Hi there, <@{user}>!")

# Start your app
if __name__ == "__main__":
    SocketModeHandler(app, os.environ["SLACK_APP_TOKEN"]).start()